import {initializeApp} from 'firebase/app';
import { getAuth, onAuthStateChanged, signInWithCustomToken } from "firebase/auth";
import {getCookie} from "./helpers";
import {token} from "morgan";

// TODO: Replace the following with your app's Firebase project configuration
const firebaseConfig = {
  apiKey: "AIzaSyBUv5Wk4BCtSlrYjPGTFnelQkpJSFB3tRY",
  authDomain: "mat3ambot.firebaseapp.com",
  projectId: "mat3ambot",
  storageBucket: "mat3ambot.appspot.com",
  messagingSenderId: "932651823561",
  appId: "1:932651823561:web:d2d0deca19293ddb607818"
};

const app = initializeApp(firebaseConfig);

const auth = getAuth();
console.log(getCookie('mat_user_id'))
console.log(getCookie('mat_user_token'))
onAuthStateChanged(auth, user => {
  if (user) {
    console.log(user)
  } else {
    if (getCookie('mat_user_token')) {
      signInWithCustomToken(auth, getCookie('mat_user_token'))
        .then(usr => console.log(usr.user))
        .catch(err => console.log('error catch => ', err))
    } else {
      // window.location.replace('/login')
      console.log('else ')
    }
  }
})
