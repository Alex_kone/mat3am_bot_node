class Restaurant {
  constructor(id, email, name, country, address, phoneNumber, isValidate) {
    this.id = id;
    this.email = email;
    this.name = name;
    this.country = country;
    this.address = address;
    this.phoneNumber = phoneNumber;
    this.isValidate = isValidate;
  }
}