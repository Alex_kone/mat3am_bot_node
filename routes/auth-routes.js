const express = require('express');
const { getLogin, postLogin, getSignup, createUser} = require('../controllers/AuthController');

const router = express.Router();

router.get('/login', getLogin);
router.post('/login', postLogin);
router.get('/signup', getSignup);
router.post('/signup', createUser);

module.exports = {
  routes: router
}
