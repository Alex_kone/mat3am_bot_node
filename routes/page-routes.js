const express = require('express');
const {homepage} = require("../controllers/PageController");
const {getCurrentUser} = require("../middlewares/authMiddleware");

const router = express.Router();

router.get('/', homepage);

module.exports = {
  routes: router
}
