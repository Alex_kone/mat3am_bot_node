const {COOKIE_USER_TOKEN} = require("../config");
const {getAuth} = require("firebase-admin/auth");
const verifyToken = async (req, res, next) => {
    const idToken = req.cookies[COOKIE_USER_TOKEN];

    if (!idToken) {
      res.redirect('/login');
      return next();
    } else {
      try {
        const decodedToken = await getAuth().verifyIdToken(idToken);
        next();
      } catch (e) {
        console.log('e => ', e);
        res.redirect('/login');
        next();
      }
    }
}

const isLogged = (req, res, next) => {
  console.log('is logged fn', req.originalUrl)
  if (req.originalUrl !== '/login' || req.originalUrl !== '/signup') {
    if (!req.user) {
      console.log('not user')
      res.redirect('/login')
      next()
    }
    console.log('not auth page', req.user)
  } else {
    next();
  }
}

const getCurrentUser = async (req, res, next) => {

  const idToken = req.cookies[COOKIE_USER_TOKEN];
  if (!idToken) {
    res.redirect('/login');
    return next()
  } else {
    try {
      const decodedToken = await getAuth().verifyIdToken(idToken);

      const queryUser = await getAuth().getUser(decodedToken.uid);
      req.user = queryUser;
      next();
    } catch (e) {
      res.redirect('/login')
      next()
    }

  }

}

module.exports = {
  verifyToken,
  getCurrentUser,
  isLogged
}
