'use strict';

const path = require('path');
const express = require('express');
const cors = require('cors');
const bodyParser = require('body-parser');
const config = require('./config');
const morgan = require('morgan');
const cookieParser = require('cookie-parser');



// Routes
const authRoutes = require('./routes/auth-routes');
const pageRoutes = require('./routes/page-routes');
const { admin } = require('./db');
const serviceAccount = require("./serviceAccount.json");

const app = express();

// admin.initializeApp({
//   credential: admin.credential.cert(serviceAccount),
//   projectId: serviceAccount.project_id
// })

// admin.initializeApp()

app.use(express.json());
app.use(express.static('public'));
app.use(morgan('dev'));
app.use(cookieParser());
app.use(cors());
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }))

app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'twig');

// app.use(isLogged)

// app.use('/api/restaurants', restaurantRoutes.routes)
// app.use('/api/users', userRoutes.routes)
app.use('/', pageRoutes.routes);
app.use('/', authRoutes.routes);

app.listen(config.port, () => console.log(`App is listening on url http://localhost:${config.port}`))
