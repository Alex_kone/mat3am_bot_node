const path = require('path');

module.exports = {
  entry: './assets/main.js',
  mode: "production",
  output: {
    path: path.resolve(__dirname, 'public'),
    filename: 'main.js'
  }
}
