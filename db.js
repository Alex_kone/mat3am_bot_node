const admin = require('firebase-admin');
const { getFirestore } = require('firebase/firestore');
const serviceAccount = require('./serviceAccount.json');
const {getAuth} = require("firebase-admin/auth");

admin.initializeApp({
  credential: admin.credential.cert(serviceAccount),
  projectId: serviceAccount.project_id
});

const db = admin.firestore();
const auth = getAuth();

const adminCreateUser = (email, password, phoneNumber ) => {
  getAuth().createUser({
    email,
    password,
    phoneNumber
  })
    .then(userRecord => console.log('userRecord => ', userRecord))
    .catch(error => console.log('error =>', error))
}

//
module.exports = { db };
