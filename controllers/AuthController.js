'use strict';

const admin = require('firebase-admin');
const { COOKIE_USER_NAME, COOKIE_USER_TOKEN} = require('../config');
const serviceAccount = require('../serviceAccount.json');
const firebaseApp = require('firebase/app');
const { firebaseConfig, adm } = require('../config');
const { getAuth, signInWithEmailAndPassword } = require('firebase/auth');
const { doc, setDoc, addDoc, collection, getFirestore } = require("firebase/firestore");
const {db} = require("../db");


firebaseApp.initializeApp(firebaseConfig);

const createUser = async (req, res, next) => {
  try {
    const {email, phoneNumber, password} = req.body;
    const userRecord = await admin.auth().createUser({email, phoneNumber, password});
    // console.log(userRecord)

    // add new user in db
    const docRef = db.collection('users').doc();
    await docRef.set({
      email: userRecord.email,
      user_id: userRecord.uid,
      phoneNumber: userRecord.phoneNumber,
    });

    console.log(docRef)

    const customToken = await admin.auth().createCustomToken(userRecord.uid)
    await res.cookie(COOKIE_USER_NAME, userRecord.uid);
    await res.cookie(COOKIE_USER_TOKEN, customToken)
    await res.redirect('/');
  } catch (e) {
    console.log('error => ', e)
    res.render('pages/auth/signup', {
      error: true,
      errorMessage: e.message
    })
  }
  //   .then(userRecord => console.log(userRecord))
  //   .catch(err => console.log('Error creating new user: ', error))
}

const signin = async (req, res, next) => {
  try {
    const { email, password } = req.body;
    // await admin.auth().
  } catch (error) {

  }
}

const getLogin = async (req, res, next) => {
  try {
    res.render('pages/auth/login');
  } catch (error) {
    res.send('il y a une erreur', error.message)
  }
}

const postLogin = async (req, res, next) => {
  const auth = getAuth();

  const { email, password } = req.body;
  console.log(req.body)
  signInWithEmailAndPassword(auth, email, password)
    .then((userCredential) => {
      // console.log('userCredential => ', userCredential.user.accessToken);
      res.cookie(COOKIE_USER_TOKEN, userCredential.user.accessToken);
      res.cookie(COOKIE_USER_NAME, userCredential.user.uid);



      res.redirect('/');
    })
    .catch(error => {
      console.log(error)
    })
}


const getSignup = async (req, res, next) => {
  res.render('pages/auth/signup');
}


module.exports = {
  createUser,
  getLogin,
  getSignup,
  postLogin
}
