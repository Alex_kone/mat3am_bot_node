'use strict';

const homepage = async (req, res, next) => {
  // if (!req.user) {
  //   res.redirect('/login');
  // } else {
  //   console.log('render homepage', req.user)
    return res.render('pages/index');
  // }
}

module.exports = {
  homepage
}
